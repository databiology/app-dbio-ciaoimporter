#
#   CIAO Importer Application Dockerfile
#

# app/dbio/base:4.2.3 is based on Ubuntu 18.04
FROM app/dbio/base:4.2.3

ENV DEBIAN_FRONTEND noninteractive

# Installing dependencies: Python3
RUN apt-get update && \
    apt-get install -y python3 && \
    rm -rf /var/lib/apt/lists/*
COPY parseInput.py /usr/local/bin/

# Entrypoint is defined as /usr/local/bin/main.sh in app/dbio/base:4.2.3
COPY main.sh  /usr/local/bin/main.sh
RUN  chmod +x /usr/local/bin/main.sh

