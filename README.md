# CIAO, Importer!

Example importer app for CIAO, this application needs to be configured as an importer app in your instance and it uses a __ciao_subject__ entity template.

Full CIAO documentation https://learn.ciao.tools/
