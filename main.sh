#!/bin/bash

# CIAO Importer main script
# Juan Caballero <juan.caballero@databiology.com>
# (C) 2020 Databiology

# Global variables definition
SCRATCH=/scratch
RESULTSDIR="$SCRATCH/results"
METADATADIR="$SCRATCH/metadata"

# Functions
falseexit () {
   echo "$1"
   exit 1
}

CMD="python3 /usr/local/bin/parseInput.py > $METADATADIR/ciaoimporter_importsheet.tsv"
bash -c "$CMD" || falseexit "Application failed"

cp "$METADATADIR/ciaoimporter_importsheet.tsv" "$RESULTSDIR/" || falseexit "Missing importsheet"

echo "Application ends"
