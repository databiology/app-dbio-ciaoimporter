# parseInput.py
#
# Example script for parsing a DBE parameters.json file and
# create an import sheet table using the ciao_subject template.
# File "parameters.json" structure is:
#  { 'APPLICATION': [ 
#         'age':'value', 
#         'sex':'value', 
#         'disease':'value',
#         'phenotype':'value',
#         'height':'value',
#         'weight':'value',
#         'cohort':'value'
#  ] }
# age and sex are mandatory, the rest are optional (projected as "null")
#
# Juan Caballero <juan.caballero@databiology.com>
# (C) 2020 Databiology

import sys
import json

param_file = "/scratch/parameters.json"

with open(param_file) as json_file:
    data = json.load(json_file)

# Values initilization
etype = "ciao_subject"
sid = ''
age = ''
sex = ''
disease = ''
phenotype = ''
height = ''
weight = ''
cohort = ''

# Table headers, first row is entity type, second is entity name
header1 = "Subject\tSubject\tSubject\tSubject\tSubject\tSubject\tSubject\tSubject\tSubject"
header2 = "IndividualId\tType\tAge\tSex\tDisease\tPhenotype\tHeight\tWeight\tCohort"
if 'APPLICATION' in data:
    if 'id' in data['APPLICATION']:
        sid = data['APPLICATION']['id']
        if sid == 'null':
            sys.stderr.write("Individual Id is missing, aborting\n")
            sys.exit()
    if 'age' in data['APPLICATION']:
        age = data['APPLICATION']['age']
        if age == 'null':
            sys.stderr.write("Age is missing, aborting\n")
            sys.exit()
    if 'sex' in data['APPLICATION']:
        sex = data['APPLICATION']['sex']
        if sex == 'null':
            sys.stderr.write("Sex is missing, aborting\n")
            sys.exit()
    if 'disease' in data['APPLICATION']:
        disease = data['APPLICATION']['disease']
        if disease == None:
            disease = ''
    if 'phenotype' in data['APPLICATION']:
        phenotype = data['APPLICATION']['phenotype']
        if phenotype == None:
            phenotype = ''
    if 'height' in data['APPLICATION']:
        height = data['APPLICATION']['height']
        if height == None:
            height = ''
    if 'weight' in data['APPLICATION']:
        weight = data['APPLICATION']['weight']
        if weight == None:
            weight = ''
    if 'cohort' in data['APPLICATION']:
        cohort = data['APPLICATION']['cohort']
        if cohort == None:
            cohort = ''

# Printing final table
print(header1)
print(header2)
print("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(sid, etype, age, sex, disease, phenotype, height, weight, cohort))

